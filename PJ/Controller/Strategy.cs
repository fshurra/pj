﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PJ.Helper;
namespace PJ.Controller
{

    public class Strategy
    {
        private int _score;
        private int _maxcount;
        private int[] _strategy;
        private string _sstrategy;

        private int genStrategy(int maxcount)
        {
            _sstrategy = "";
            _strategy = new int[maxcount];
            //Random ra = new Random();
            long tick = DateTime.Now.Ticks;
            Random ra = new Random((int)(tick));
            for (int i = 0; i < maxcount; i++)
            {
                int rand = ra.Next() % 2;
                if (rand == 1)
                {
                    _strategy[i] = 1;
                    _sstrategy = _sstrategy + "1";
                }
                else
                {
                    _strategy[i] = -1;
                    _sstrategy = _sstrategy + "2";
                }
            }
            return 0;
        }

        public int Score
        {
            get
            {
                return _score;
            }
        }

        public string getSStrategy()
        {
            return _sstrategy;
        }

        public Strategy(int maxcount )
        {
            //_strategy = new int[maxcount];
            genStrategy(maxcount);
            _maxcount = maxcount;
        }

        public int getResponse(string memory) //Input is a memory
        {
            int index = MathHelper.binstring2dec(memory);
            return _strategy[index];
        }

        public int refreshScore(string memory, int winningchoice)
        {
                // The choice is the winner side
                int index = MathHelper.binstring2dec(memory);
                int inf = _strategy[index];
                if (inf == winningchoice)
                {
                    _score += 1;
                }
            return 1;
        }
    }
}
