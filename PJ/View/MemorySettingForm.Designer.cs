﻿namespace PJ.View
{
    partial class MemorySettingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataSettingMemory = new System.Windows.Forms.DataGridView();
            this.buttonMemoryClear = new System.Windows.Forms.Button();
            this.buttonMemorySave = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataSettingMemory)).BeginInit();
            this.SuspendLayout();
            // 
            // dataSettingMemory
            // 
            this.dataSettingMemory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataSettingMemory.Location = new System.Drawing.Point(3, -1);
            this.dataSettingMemory.Name = "dataSettingMemory";
            this.dataSettingMemory.Size = new System.Drawing.Size(624, 364);
            this.dataSettingMemory.TabIndex = 0;
            // 
            // buttonMemoryClear
            // 
            this.buttonMemoryClear.Location = new System.Drawing.Point(384, 371);
            this.buttonMemoryClear.Name = "buttonMemoryClear";
            this.buttonMemoryClear.Size = new System.Drawing.Size(108, 34);
            this.buttonMemoryClear.TabIndex = 1;
            this.buttonMemoryClear.Text = "Memory clear";
            this.buttonMemoryClear.UseVisualStyleBackColor = true;
            this.buttonMemoryClear.Click += new System.EventHandler(this.buttonMemoryClear_Click);
            // 
            // buttonMemorySave
            // 
            this.buttonMemorySave.Location = new System.Drawing.Point(508, 376);
            this.buttonMemorySave.Name = "buttonMemorySave";
            this.buttonMemorySave.Size = new System.Drawing.Size(109, 29);
            this.buttonMemorySave.TabIndex = 2;
            this.buttonMemorySave.Text = "Memory save";
            this.buttonMemorySave.UseVisualStyleBackColor = true;
            this.buttonMemorySave.Click += new System.EventHandler(this.buttonMemorySave_Click);
            // 
            // MemorySettingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(629, 410);
            this.Controls.Add(this.buttonMemorySave);
            this.Controls.Add(this.buttonMemoryClear);
            this.Controls.Add(this.dataSettingMemory);
            this.Name = "MemorySettingForm";
            this.Text = "MemorySettingForm";
            ((System.ComponentModel.ISupportInitialize)(this.dataSettingMemory)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataSettingMemory;
        private System.Windows.Forms.Button buttonMemoryClear;
        private System.Windows.Forms.Button buttonMemorySave;
    }
}