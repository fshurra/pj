﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PJ;
using PJ.Model;
using PJ.Controller;
namespace PJ.View
{
    public partial class MainForm : Form
    {
        private Game _currentGame;
        private bool autoRun=false;
        public MainForm()
        {
            InitializeComponent();
            SettingForm setting = new SettingForm(this);
            this.Enabled = false;
            setting.ShowDialog();
        }
        public void SetGame(GameConfigModel config){
            _currentGame=new Game(config);
            autoRun = false;
        }
        private void MainForm_Load(object sender, EventArgs e)
        {
                   
        }

        private void buttonStatistics_Click(object sender, EventArgs e)
        {
            StatisticsForm statisticForm = new StatisticsForm();
            statisticForm.Show();
        }

        private void buttonSetting_Click(object sender, EventArgs e)
        {
            SettingForm setting = new SettingForm(this);
            setting.ShowDialog();
        }

        private void buttonMemorySetting_Click(object sender, EventArgs e)
        {
            MemorySettingForm memorySetting = new MemorySettingForm();
            memorySetting.ShowDialog();
        }
        private void autoChoose()
        {
            PlayerInfoModel playerInfo = _currentGame.getHumanPlayerInfo();
            _currentGame.doNextRound(playerInfo.recommendChoice);
            
        }
        private void buttonAutoSwitch_Click(object sender, EventArgs e)
        {
            if (!autoRun)
            {
                autoRun = true;
                buttonAutoSwitch.Text = "Stop";
                timer1.Start();
                buttonSetting.Enabled = false;
                buttonReset.Enabled = false;
                buttonMemorySetting.Enabled = false;

            }
            else if (autoRun)
            {
                autoRun = false;
                buttonAutoSwitch.Text = "AutoRun";
                timer1.Stop();
                buttonSetting.Enabled = true;
                buttonReset.Enabled = true;
                buttonMemorySetting.Enabled = true;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            autoChoose();
        }

        private void buttonGoBar_Click(object sender, EventArgs e)
        {
            _currentGame.doNextRound(1);
        }

        private void buttonStayHome_Click(object sender, EventArgs e)
        {
            _currentGame.doNextRound(-1);
        }
    }
}
