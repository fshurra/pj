﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game1
{
    class MathHelper
    {
        public static int binstring2dec(string bins)
        {
            int length = bins.Length;
            int dec = 0;

            for (int i = 0; i < length; i++)
            {
                int multi = length - i - 1;
                if (bins[i] == '1')
                {
                    dec = dec + Convert.ToInt32(Math.Pow(2, multi));
                }
            }
            return dec;
        }
    }
}
